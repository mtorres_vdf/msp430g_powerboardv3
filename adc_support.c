/*
 * adc_support.c
 *
 *  Created on: Jun 29, 2014
 *      Author: mtorres
 */
#include <msp430.h>
#include <stdint.h>
//#include "adc_support.h"

//interger to ascii code. Not mine, found it on 43oh....thought I had grabbed the user name to attribute it...
static char *i2a(unsigned i, char *a, unsigned r)
  {
    if (i/r > 0) a = i2a(i/r,a,r);
    *a = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"[i%r];
    return a+1;
  }

//This is the method used. Method parms are as follows
// int i = integer value to convert
// char a char pointer to store value
// int r Radix to use(10 would be like base 10 I think)

char *itoa(int i, char *a, int r)
  {
    if ((r < 2) || (r > 36)) r = 10  ;
    if (i < 0)
    {
        *a = '-';
        *i2a(-(unsigned)i,a+1,r) = 0;
    }
    else *i2a(i,a,r) = 0;
    return a;
  }

void initAdc(){
	ADC10CTL0 = ADC10SHT_2 + ADC10IE + ADC10ON + MSC;//single sample also leaving default vref to use the vcc/vss
	ADC10CTL1 = INCH_4 + CONSEQ_1;
	ADC10AE0 |= BIT2+BIT3+BIT4;
	ADC10DTC1 = 3;//This is turning on DTC be aware of how it handles the data differently!
}

void enableAdc(volatile uint16_t *foo){
	ADC10SA = (uint16_t)foo;
	ADC10CTL0 |=ENC + ADC10SC;
    __bis_SR_register(CPUOFF + GIE);        // LPM0, ADC10_ISR will force exit
}

void disableAdc(){
	ADC10CTL0 &= ~ENC;
	while (ADC10CTL1 & BUSY);               // Wait if ADC10 core is active
}

#pragma vector=ADC10_VECTOR
__interrupt void adc10_doStuff(void){
//disableAdc();
_low_power_mode_off_on_exit();
}
