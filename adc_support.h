/*
 * adc_support.h
 *
 *  Created on: Jun 29, 2014
 *      Author: mtorres
 */

#ifndef ADC_SUPPORT_H_
#define ADC_SUPPORT_H_


static char *i2a(unsigned i, char *a, unsigned r);
char *itoa(int i, char *a, int r);

void initAdc();
void enableAdc(volatile uint16_t *foo);
void disableAdc();

#endif /* ADC_SUPPORT_H_ */
