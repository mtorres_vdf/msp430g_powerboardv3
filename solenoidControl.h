/*
 * solendoidControl.h
 *
 *  Created on: Mar 22, 2019
 *      Author: voodo_000
 */

#ifndef SOLENOIDCONTROL_H_
#define SOLENOIDCONTROL_H_

#include <stdint.h>

#include <wdt_delay.h>

typedef struct {
    uint8_t isSOLOn;
}SOLStatus;
void putc(unsigned);
void puts(char *);
void Red_Off(void);
void Red_On(void);
void Orange_On(void);
void Orange_Off(void);
void Slnd_On(void);//Turn solenoid on.
void Slnd_Off(void); //Turn off solenoid

void TurnSolOn(SOLStatus *ss, uint8_t onTime);
void TurnSolOff(SOLStatus *ss);

void TurnSolOn(SOLStatus *ss, uint8_t onTime){
    ss->isSOLOn=1;
    uint8_t onTimeCounter;
    Slnd_On();
    for(onTimeCounter=0; onTimeCounter < onTime; onTimeCounter++){
        Red_On();
        WD_intervalTimerInit(10,2);
        Red_Off();
        WD_intervalTimerInit(10,2);
    }
}

void TurnSolOff(SOLStatus *ss){
    ss->isSOLOn=0;
    Slnd_Off();
}

#endif /* SOLENOIDCONTROL_H_ */
