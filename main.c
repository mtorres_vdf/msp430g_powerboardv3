#include <msp430.h> 
#include <stdint.h>
#include "wdt_delay.h"
#include "adc_support.h"
#include "solenoidControl.h"
#include <string.h>


/**
 * main.c
 */
#define MOISTURE_MIDLVL 450
#define LIGHT_MIDLVL 600
#define VOLTAGE_MIDLVL 410 //440 seemed like good bright light.
#define BUFFERARRAYCOUNT 3
#define SYS_MESSAGE "{ \"System \" :"
#define GD_MESSAGE " \"Nominal\" }"
#define LL_MESSAGE " \"Light_LOW\" }"
#define SPL_MESSAGE " \"Solar_Power_LOW\" }"
#define SLND_ON_MESSAGE " \"Slnd_ON\" }"
#define SLND_OFF_MESSAGE " \"Slnd_OFF\" }"
#define SLRPWR_OK_MESSAGE " \"Solar_Power_OK\" }" //may not need

#define ADC_MOISTURR_ID 0
#define ADC_LIGHT_ID 1
#define ADC_VOLTAGE_ID 2


//#define LL_MESSAGE "{ \"System \" : \"Light_LOW\" }"
//#define SPL_MESSAGE "{ \"System \" : \"Solar_Power_LOW\" }"


void buildJson(char *data, char *counter);
void initMainClock();

void sendStatus(char *data);
    // Functions in serial.asm (this really should be in a header file)
    //void serial_setup(unsigned out_mask,unsigned in_mask, unsigned duration);
    void serial_setup(unsigned out_mask, unsigned duration);
    void putc(unsigned);
    void puts(char *);
    //LED Controls
    void Red_Off(void);
    void Red_On(void);
    void Orange_Off(void);
    void Orange_On(void);
    void Green_On(void);
    void Green_Off(void);
    void All_Off(void);

    volatile uint16_t adcData[3]; //this needs to match the DTC data register
    uint8_t runProc_01 = 0;
    uint8_t LowM_Counter = 0;
    char ADCbuffer[BUFFERARRAYCOUNT]; //buffer for conversion from I to C
    char LCbuffer[1]; //buffer for conversion from I to C
    SOLStatus solStat;
    uint16_t sendBuffer[BUFFERARRAYCOUNT];
    const uint8_t buffCnt = 0;
    uint8_t sleepTimeCounter = 0;
int main(void)
{

    initMainClock();
    StopWD();
    initAdc();
    serial_setup(BIT1, 1000000 / 9600);
    P1OUT &= ~(BIT6 + BIT5 + BIT7 +BIT0);
    P1DIR |= (BIT6+ BIT5 + BIT7+BIT0);
    Orange_On();
    _BIS_SR(GIE); //LPM0
    uint8_t msgSend = 12;
    uint8_t timeRun = 30;
    while(1){
/*


*/
        (msgSend > 0) ? (msgSend--) : (msgSend = 12);
        if(runProc_01 == 2){

            sendStatus(SYS_MESSAGE LL_MESSAGE);
            for(sleepTimeCounter=50; sleepTimeCounter > 0; sleepTimeCounter--){
                Red_On();
                Green_Off();
                WD_intervalTimerInit(10,1);
                Red_Off();
                Green_On();
                WD_intervalTimerInit(30,2);
                }
            Green_Off();
            runProc_01 = 0;
            }

        if(runProc_01 == 1){
            buildJson(ADCbuffer, LCbuffer);//spit out the data in json format to the serial port.
            runProc_01 = 0;
            }
        if(runProc_01 == 0){
        __no_operation();
        enableAdc(adcData);
        __no_operation();


        //check to see if adcData0 is below threshold
        if (adcData[ADC_MOISTURR_ID] < MOISTURE_MIDLVL && adcData[ADC_VOLTAGE_ID] >= VOLTAGE_MIDLVL){
            Green_On();
            sendStatus(SYS_MESSAGE GD_MESSAGE);
            LowM_Counter = 0;//reset the counter if its flipping between the M_MID value until it's consistent

            if (msgSend == 1) { sendStatus(SYS_MESSAGE GD_MESSAGE);}

            }
        else{Red_On();}
            if(LowM_Counter > 10)
                {
                if (adcData[ADC_VOLTAGE_ID] >= VOLTAGE_MIDLVL){
                    sendStatus(SYS_MESSAGE SLND_ON_MESSAGE); //Solar_Power_LOW
                    TurnSolOn(&solStat, 500);
                    TurnSolOff(&solStat);
                    sendStatus(SYS_MESSAGE SLND_OFF_MESSAGE); //Solar_Power_LOW
                    }
                else{
                    sendStatus(SYS_MESSAGE SPL_MESSAGE); //Solar_Power_LOW
                    }

                LowM_Counter = 0; //clear counter and let count up again
                }
            else{LowM_Counter++;}

        memcpy(&sendBuffer[buffCnt], &adcData,6); //This is 6 as each uint16_t value is 2 bytes each. The array storage is for 16 bit values memcpy is 8bit
        WD_intervalTimerInit(10,1); //30,2
        Red_Off();
        Green_Off();
        //_low_power_mode_0();
        disableAdc();


        (adcData[ADC_LIGHT_ID] < LIGHT_MIDLVL) ? (timeRun = 100) : (timeRun = 40);
        WD_intervalTimerInit(timeRun,2);//set to 20,4 for no crystal

/*
        buffCnt +=3;
        if(buffCnt>=BUFFERARRAYCOUNT) {
            buffCnt = 0;
            runProc_01 = 1;
            }
*/
        //runProc_01 = 1; //moved this out of the loop above.
        (msgSend == 2 || msgSend == 8) ? (runProc_01 = 1) : (runProc_01 = 0);

        }
        //check after rp0 has run and make sure it's not in mode 1
        if (adcData[ADC_LIGHT_ID] < LIGHT_MIDLVL && msgSend == 5){
                   runProc_01 = 2;
               }
        //else {runProc_01 = 0;}
    }
	return 0;
}


void initMainClock(){
    // Use 1 MHz DCO factory calibration
    DCOCTL = 0;
    BCSCTL1 = CALBC1_1MHZ;
    DCOCTL = CALDCO_1MHZ;
    BCSCTL3 |= LFXT1S_2;
    BCSCTL1 |= DIVA_0;
}

//For this function count should be sizeof -->data it is hard coded at the moment
void buildJson(char *data, char *counter){
    Orange_Off();
    WD_intervalTimerInit(3,1);

    puts("{");
        putc(13);
        putc(10);
        uint8_t ic;
        for(ic = 0; ic <=sizeof(data)/sizeof(data[0]); ic++){

            puts("\"ADCDAT-");
            itoa((ic), counter, 10);
            puts(counter);
            puts("\" : ");
            itoa((sendBuffer[ic]), data, 10);
            sendBuffer[ic] = 0; //clear the data as it's read
            puts(data);
            if(ic < sizeof(data)/sizeof(data[0])){
            puts(", ");
            }
            putc(13);
            putc(10);
            }
        puts("}");
        putc(13);
        putc(10);
        ic = 0; //reset counter
        Orange_On();
        WD_intervalTimerInit(3,1);

}

void sendStatus(char *data){
    Orange_Off();
    WD_intervalTimerInit(3,1);
    puts(data);
    putc(13);
    putc(10);
    Orange_On();
    WD_intervalTimerInit(3,1);
}
